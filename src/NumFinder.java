/**
 * Created by mjlobo on 25/11/2017.
 */
public class NumFinder {
    static int maxValue = 5526;
    static int minValue = -273;
    static String wrongValueMessage = "Wrong Value ";
    public static int find(double[] list) throws IllegalArgumentException {
        double closerValue = Double.MAX_VALUE;
        if (list.length == 0) {
            return 0;
        }
        else {
            for (int i=0; i<list.length; i++) {
                if (list[i]%1!=0){
                    throw new IllegalArgumentException(wrongValueMessage+list[i]);

                }
                else {
                    if (list[i] < minValue || list[i] > maxValue) {
                        throw new IllegalArgumentException(wrongValueMessage + list[i]);
                    }
                    else {
                        if (Math.abs(closerValue) == Math.abs(list[i])) {
                            if (list[i]>closerValue) {
                                closerValue = list[i];
                            }
                        }
                        else {
                            if (Math.abs(closerValue) > Math.abs(list[i])) {
                                closerValue = list[i];
                            }
                        }
                    }
                }

            }
        }
     return (int)closerValue;

    }
}
