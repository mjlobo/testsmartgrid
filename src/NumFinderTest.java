/**
 * Created by mjlobo on 25/11/2017.
 */
import org.hamcrest.Matcher;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.regex.Pattern;

public class NumFinderTest {
    @Test
    public final void whenEmptyListIsUsedThenReturnValueIs0() {
        Assert.assertEquals(0, NumFinder.find(new double[]{}));
    }

    @Rule
    public ExpectedException illegalArgumentException = ExpectedException.none();

    @Test
    public final void whenNumbersOutsideRangeThrowException() {
        illegalArgumentException.expect(IllegalArgumentException.class);
        NumFinder.find(new double[] {-300, 5600});
    }

    @Test
    public final void whenNumberIsNoIntegerThrowException() {
        illegalArgumentException.expect(IllegalArgumentException.class);
        NumFinder.find(new double[] {-0.5, 1000});
    }

    @Test
    public final void whenNumbersAreInRangeNoExceptionIsThrown() {
        NumFinder.find(new double [] {-272, 5525});
    }

    @Test
    public final void findsNumberCloserTo0() {
        Assert.assertEquals(1, NumFinder.find(new double[] {2,4,5,1,6,2,5,10}));
    }

    @Test
    public final void findsNumberCloserTo0WithNegativeNumbers() {
        Assert.assertEquals(-1, NumFinder.find(new double[] {2,-1,5,2,6,2,5,10}));
    }

    @Test
    public final void findsNumberCloserTo0RepeatedNumbers() {
        Assert.assertEquals(1, NumFinder.find(new double[] {2,4,-1,1,6,-1,5,10}));
    }





}
